﻿using UnityEngine;
using System.Collections;

public class GiftScript : MonoBehaviour {

    public bool isFull = false;
    Animator anim;

    // Use this for initialization
    void Start()
    {
        anim = GetComponent<Animator>();

    }

    // Update is called once per frame
    void Update()
    {
        anim.SetBool("isFull", isFull);
    }
}
