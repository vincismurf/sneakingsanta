﻿using UnityEngine;
using System.Collections;

public class BabyAI : MonoBehaviour {

    public float maxSpeed = 1.5f;
    public int thinkTime = 5;
    public float sight = 4;
    public bool DEBUG = true;

    bool facingRight = true;
    Time lastTime;
    float counter = 0;
    bool scared = false;
    enum BABYSTATE { IDLE, WALKING, CRYING };

    private Rigidbody2D rB = null;
    
    Vector3 rayStart, rayEnd;
    BABYSTATE state;
    Animator anim;
    /*
    * Use this for initialization
    */
	void Start () {
        anim = GetComponent<Animator>();
        state = BABYSTATE.IDLE;
        rB = GetComponent<Rigidbody2D>();
        if (rB)
        {
            rB.velocity = transform.forward;
        }
    }
    /*
    * Update is called once per frame
    */
	void FixedUpdate() {
        lookAround();
        updateThink();
        updateMove();
        updateAnimator();
        drawDebug();
	}

    /*
     * 
     */
    void updateThink()
    {
        counter += Time.deltaTime;
        int t = (int)(counter % thinkTime);

        if (t == 0)
        {
            if(scared)
            {
                Debug.Log("I saw santa!");
                GameObject.Find("Santa").GetComponent<playerController>().SantaCaught();

            }
        }

    }
    /*
     * Lets raycast each tick
     */
    void lookAround()
    {
        rayStart = gameObject.transform.position;

        if (facingRight)
            rayEnd = rayStart + new Vector3( sight, 0, 0);
        else
            rayEnd = rayStart + new Vector3(-sight, 0, 0);

        bool something = Physics2D.Linecast(rayStart, rayEnd,1 <<LayerMask.NameToLayer("Santa"));
        // If you are close, is santa using magic?
        if(something == true)
        {
            something = !GameObject.Find("Santa").GetComponent<playerController>().useMagic;// heh ugly boolean syntax
        }

        scared = something;

    }
    /*
     * Go appropriate direction unless we saw santa
     */
    private void updateMove()
    {
        
        Vector3 nextV = Vector3.zero; 
        if (facingRight)
            nextV = transform.right * maxSpeed;
        else
            nextV = -transform.right * maxSpeed;

        if (scared)
        {
            nextV = Vector3.zero;
        }
        //Debug.Log("baby update nextV " + nextV);
        rB.MovePosition(transform.position + nextV * Time.deltaTime);
        rB.velocity = nextV;
    }
    /*
     * Update the Baby values 
     */
    private void updateAnimator()
    {
        anim.SetFloat("hSpeed", Mathf.Abs(rB.velocity.x));
        anim.SetBool("alarmed", scared);
    }
    /*
     * 
     */
    void Flip()
    {
        facingRight = !facingRight;
        Vector3 theScale = transform.localScale;
        theScale.x *= -1;
        transform.localScale = theScale;
    }
    /*
     * Turn Baby around
     */
    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.tag == "Gift")
        {
            Flip();
        }

        if (other.gameObject.tag == "Tree")
        {
            Flip();
        }
    }
    /*
     * 
     */
    private void drawDebug()
    {
        if (DEBUG)
        {
            if (scared)
                Debug.DrawLine(rayStart, rayEnd, Color.red);
            else
                Debug.DrawLine(rayStart, rayEnd, Color.green);
        }
    }
}
