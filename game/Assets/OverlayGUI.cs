﻿using UnityEngine;
using System.Collections;

public class OverlayGUI : MonoBehaviour {
 
    public Texture aTexture;
    private Rect rect;
	// Use this for initialization
	void Start () {
        rect = new Rect(0, 0, Screen.width, Screen.height);
	}
	
	// Update is called once per frame
	void Update () {
        rect = new Rect(0, 0, Screen.width, Screen.height);
	}
   
    void OnGUI()
    {
        if (!aTexture)
        {
            Debug.LogError("Assign a Texture in the inspector.");
            return;
        }
        GUI.DrawTexture(rect, aTexture, ScaleMode.ScaleToFit, true);
    }

}
