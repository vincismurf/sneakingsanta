﻿using UnityEngine;
using System.Collections;

public class MenuItemGUI : MonoBehaviour {

    public Canvas _gui;
    public GameObject _image;
    public Camera _camera;

    public bool enabled = false;

    public Canvas gui
    {
        //set the person name
        set { this._gui = value; }
        //get the person name 
        get { return this._gui; }
    }

    public GameObject image
    {
        //set the person name
        set { this._image = value; }
        //get the person name 
        get { return this._image; }
    }

    public Camera camera
    {
        //set the person name
        set { this._camera = value; }
        //get the person name 
        get { return this._camera; }
    }
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    public void Off()
    {
        enabled = false;
        if (camera)
            camera.enabled = enabled;
        if (image)
            image.SetActive(enabled);
        if (gui)
            gui.enabled = enabled;
    }

    public void On()
    {
        enabled = true;
        if (camera)
            camera.enabled = enabled;
        if (image)
            image.SetActive(enabled);
        if (gui)
            gui.enabled = enabled;
      
    }
}
