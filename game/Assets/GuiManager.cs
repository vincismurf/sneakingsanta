﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;


public class GuiManager : MonoBehaviour {

    public MenuItemGUI[] items; 
    public Text debugText;
    private RectTransform debugMenutransform;
    public enum MenuState { MainMenu, Loading, InGame, Prize, Social };
	// Use this for initialization
	void Start () {
        debugMenutransform = debugText.rectTransform;
	}
	
	// Update is called once per frame
	void Update () {
	
	}


    /*
     *  WATCH THE ORDER IN EDITOR!!!!
     */
    internal void show(MenuState p)
    {
        switch (p)
        {
            case MenuState.InGame: load(items[1]);
                break;
            case MenuState.Social: load(items[2]);
                break;
            case MenuState.MainMenu: load(items[0]);
                break;
            default: Debug.Log("MenuItem " + p + " Not Supported");
                break;
        }

    }
    private void load(MenuItemGUI item)
    {
        for (int i = 0; i < items.Length; i++)
        {
            MenuItemGUI m = items[i];
            if (m == item)
                m.On();
            else
                m.Off();
        }
        setDebug(item);
    }

    /*
     * Simple Panel that is switched to the current canvas
     */
    private void setDebug(MenuItemGUI item)
    {
        Canvas c = item.GetComponent<MenuItemGUI>().gui;
        Vector3 oldScale = debugText.rectTransform.localScale;
        debugText.rectTransform.parent = c.GetComponent<RectTransform>();
        debugText.rectTransform.localScale = oldScale;
        debugText.rectTransform.SetAsFirstSibling();
        if (item == items[1])
        {
            debugText.rectTransform.localScale = new Vector3(1, 1, 1);
            debugText.rectTransform.localPosition = new Vector3(-450, 0, 0);
        }
        else
        {
            debugText.rectTransform.localScale = new Vector3(2, 2, 2);
            debugText.rectTransform.localPosition = new Vector3(-390.439f, -202.04f, 20f);
        }
    }
    public void updateDebug(string s)
    {
        debugText.text = s;
    }
}
