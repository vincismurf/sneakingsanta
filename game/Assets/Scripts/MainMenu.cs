﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
//using Parse;
using System;

using System.Linq;

public class MainMenu : MonoBehaviour {

	public GUISkin menuSkin;

	public GameObject loggedInUIElements;
	public GameObject loggedOutUIElements;

	public GameObject profilePicture;
	public GameObject nameLabel;
	public GameObject locationLabel;
	public GameObject genderLabel;
	public GameObject birthdayLabel;
	public GameObject relationshipLabel;
	public GameObject welcomeLabel;

	public Texture defaultProfilePictureTexture;

	private TextMesh nameLabelMesh;
	private TextMesh locationLabelMesh;
	private TextMesh genderLabelMesh;
	private TextMesh birthdayLabelMesh;
	private TextMesh relationshipLabelMesh;
	private MeshRenderer profilePictureRenderer;

	Rect loginButtonRect;
	Rect logoutButtonRect;

	private float TextureScale
	{
		get
		{
			#if UNITY_EDITOR
			return 1.0f;
			# else
			return 2.0f;
			#endif
		}
	}

	// Use this for initialization
	void Start () {
		nameLabelMesh = nameLabel.GetComponent<TextMesh>();
		locationLabelMesh = locationLabel.GetComponent<TextMesh>();
		genderLabelMesh = genderLabel.GetComponent<TextMesh>();
		birthdayLabelMesh = birthdayLabel.GetComponent<TextMesh>();
		relationshipLabelMesh = relationshipLabel.GetComponent<TextMesh>();
		profilePictureRenderer = profilePicture.GetComponent<MeshRenderer>();

		float loginButtonWidth =TextureScale * 180.0f;
		float loginButtonHeight = TextureScale * 38.0f;
		float loginButtonX = (Screen.width - loginButtonWidth) / 2.0f;
		loginButtonRect = new Rect (loginButtonX,10,loginButtonWidth,loginButtonHeight);

		float logoutButtonWidth = TextureScale * 90.0f;
		float logoutButtonHeight = TextureScale * 38.0f;
		float logoutButtonX = (Screen.width - logoutButtonWidth) / 2.0f;
		logoutButtonRect = new Rect (logoutButtonX,10,logoutButtonWidth,logoutButtonHeight);


	}

	void Awake()
	{
		enabled = false;

	}

	private void SetInit()
	{
		enabled = true;
	}

	private void OnHideUnity(bool isGameShown) {
		if (!isGameShown)
		{
			// pause the game - we will need to hide
			Time.timeScale = 0;
		}
		else
		{
			// start the game back up - we're getting focus again
			Time.timeScale = 1;
		}
	}

	void OnGUI() {
		GUI.skin = menuSkin;

		// Check if no Parse user or not logged into Facebook

	}

	private void FBLogin() {

	}

	private void FBLoginCallback() {

	}

	private void ParseFBLogout() {

		//ParseUser.LogOut();
		showLoggedOut();
	}

	private void FBAPICallback()
	{

	}

	//private IEnumerator saveUserProfile(Dictionary<string, string> profile) {
	//	var user = ParseUser.CurrentUser;
	//	user["profile"] = profile;
	//	// Save if there have been any updates
	//	if (user.IsKeyDirty("profile")) {
	//		var saveTask = user.SaveAsync();
	//		while (!saveTask.IsCompleted) yield return null;
	//		UpdateProfile();
	//	}
	//}

	private string getDataValueForKey(Dictionary<string, object> dict, string key) {
		object objectForKey;
		if (dict.TryGetValue(key, out objectForKey)) {
			return (string)objectForKey;
		} else {
			return "";
		}
	}

	private void UpdateProfile() {
		// Display cached info
		//var user = ParseUser.CurrentUser;
		//IDictionary<string, string> userProfile = user.Get<IDictionary<string, string>>("profile");
		//nameLabelMesh.text = ResolveTextSize(userProfile["name"], 12); // Check and wrap words
		//locationLabelMesh.text = userProfile.ContainsKey("location") ? ResolveTextSize(userProfile["location"], 25) : "";
		//genderLabelMesh.text = userProfile.ContainsKey("gender") ? userProfile["gender"] : "";
		//birthdayLabelMesh.text = userProfile.ContainsKey("birthday") ? userProfile["birthday"] : "";
		//relationshipLabelMesh.text = userProfile.ContainsKey("relationship") ? userProfile["relationship"] : "";
		//StartCoroutine("UpdateProfilePictureTexture", userProfile["pictureURL"]);
	}

	private IEnumerator UpdateProfilePictureTexture(string pictureURL)
	{

		yield return null;

	}

	private void showLoggedIn() {
		foreach (Transform child in loggedOutUIElements.transform)
		{
			child.GetComponent<Renderer>().enabled = false;
		}
		foreach (Transform child in loggedInUIElements.transform)
		{
			child.GetComponent<Renderer>().enabled = true;
		}
	}

	private void showLoggedOut() {
		foreach (Transform child in loggedInUIElements.transform)
		{
			child.GetComponent<Renderer>().enabled = false;
		}
		foreach (Transform child in loggedOutUIElements.transform)
		{
			child.GetComponent<Renderer>().enabled = true;
		}
		profilePictureRenderer.materials[0].mainTexture = defaultProfilePictureTexture;
	}

	// Wrap text by line height
	private string ResolveTextSize(string input, int lineLength){
		
		// Split string by char " "    
		string[] words = input.Split(" "[0]);
		
		// Prepare result
		string result = "";
		
		// Temp line string
		string line = "";
		
		// for each all words     
		foreach(string s in words){
			// Append current word into line
			string temp = line + " " + s;
			
			// If line length is bigger than lineLength
			if(temp.Length > lineLength){
				
				// Append current line into result
				result += line + "\n";
				// Remain word append into new line
				line = s;
			}
			// Append current word into current line
			else {
				line = temp;
			}
		}
		
		// Append last line into result   
		result += line;
		
		// Remove first " " char
		return result.Substring(1,result.Length-1);
	}
	
}
