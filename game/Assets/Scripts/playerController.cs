﻿using UnityEngine;
using System.Collections;

public class playerController : MonoBehaviour {

    public float maxSpeed = 2f;
    bool facingRight = true;
    bool grounded = false;
    public Transform groundCheck;
    float groundRadius = 0.2f;
    public LayerMask whatIsGround;
    public float jumpForce = 700;
    public bool useMagic = false;
    public Camera cam;
    Animator anim;
    GameObject[] babies;
    private GameObject controller;
    private Rigidbody2D rB = null;

	// Use this for initialization
	void Start () {
        anim = GetComponent<Animator>() ;
        babies = GameObject.FindGameObjectsWithTag("Baby");
        controller = GameObject.Find("GameController");
        rB = GetComponent<Rigidbody2D>();
       // cam = GameObject.Find("MainCamera").GetComponent<Camera>();
    }
    // Update is called once per frame
    void Update()
    {
        //Do Vertical physics
        // remap 1:01:00 ish remap a to input manager fore repamming
        if (grounded && Input.GetKeyDown(KeyCode.Space))
        {
            Debug.Log("jump");

            anim.SetBool("ground", false);
            rB.AddForce(new Vector2(0, jumpForce));
        }

        if (Input.GetKeyDown("m"))
        {
            useMagic = true;
            print("m key was pressed");
        }

        updateCollision();
        if (cam != null)
        cam.transform.position = new Vector3(transform.position.x, transform.position.y, -10f);

    }

    private void updateCollision()
    {

       foreach (GameObject baby in babies)
      {
                 BoxCollider2D box = baby.GetComponent<BoxCollider2D>();
                 CircleCollider2D circle = baby.GetComponent<CircleCollider2D>();
                 BoxCollider2D boxSanta = GetComponent<BoxCollider2D>();
                 CircleCollider2D circleSanta = GetComponent<CircleCollider2D>();

                 Physics2D.IgnoreCollision(box, boxSanta, useMagic);
                 Physics2D.IgnoreCollision(circle, circleSanta, useMagic);
                 Physics2D.IgnoreCollision(box, circleSanta, useMagic);
                 Physics2D.IgnoreCollision(circle, boxSanta, useMagic);
       }

    }



	void FixedUpdate () {

        //Pre tests and Input (grab the state) 
        grounded = Physics2D.OverlapCircle(groundCheck.position, groundRadius, whatIsGround);
        float move = Input.GetAxis("Horizontal");

        if (Mathf.Abs(move) > 0)
            useMagic = false;

        //set Animation bools
        anim.SetBool("useMagic", useMagic);
        anim.SetBool("ground", grounded);
        anim.SetFloat("vSpeed", GetComponent<Rigidbody2D>().velocity.y);
        anim.SetFloat("hSpeed", Mathf.Abs(move));




        //Do Horizonal physics
        rB.velocity = new Vector2(move * maxSpeed, rB.velocity.y);
       // cam. = new Vector3(move * maxSpeed, rigidbody2D.velocity.y,0);
        if (move > 0 && !facingRight)
            Flip();
        else if (move < 0 && facingRight)
            Flip();
    }

    void Flip()
    {
        facingRight = !facingRight;
        Vector3 theScale = transform.localScale;
        theScale.x *= -1;
        transform.localScale = theScale;
    }

    void OnTriggerEnter2D(Collider2D other)
    {
       
        if (other.gameObject.tag == "Gift")
        {
            Debug.Log("You entered a gift");
            other.GetComponent<GiftScript>().isFull = true;
        }

        if (other.gameObject.tag == "Tree")
        {
            Debug.Log("You Win!");
            other.GetComponent<GiftScript>().isFull = true;
            controller.GetComponent<GameState>().GameWin();
        }
    }

    public void SantaCaught()
    {
        Debug.Log("Sants CAught");

    }
}
