function AjaxCaller() {
    var xmlhttp = false;
    try {
        xmlhttp = new ActiveXObject("Msxml2.XMLHTTP");
    } catch (e) {
        try {
            xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
        } catch (E) {
            xmlhttp = false;
        }
    }

    if (!xmlhttp && typeof XMLHttpRequest != 'undefined') {
        xmlhttp = new XMLHttpRequest();
    }
    return xmlhttp;
}

function callPage(url, div) {
    ajax = AjaxCaller();
    ajax.open("GET", url, true);
    ajax.onreadystatechange = function () {
        if (ajax.readyState == 4) {
            if (ajax.status == 200) {
                div.innerHTML = ajax.responseText;
            }
        }
    }
    ajax.send(null);
}

function call(year_id, event_id) {
    var year = document.getElementById(year_id).value;
    var event = document.getElementById(event_id).value;

    callPage('connect.php?id=' + year + '&' + 'pwd=+' + event, document.getElementById(targetId));
}